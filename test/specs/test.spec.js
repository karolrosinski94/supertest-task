import request from "supertest";

const url = "https://api.dropboxapi.com/2";
const token =
    "myToken";
const requestUrl = request(url);

describe("Dropbox API test", function () {
    it("estabilish connection", function () {
        try {
            requestUrl
                .post("/check/app")
                .set("Authorization", `Bearer ${token}`)
                .expect(200);
        } catch (error) {
            console.log(error);
        }
    });
});
